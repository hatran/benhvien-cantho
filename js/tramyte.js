var datatramyte = [
 {
   "STT": 1,
   "Name": "Trạm Y tế NT Sông Hậu",
   "address": "Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.1324698,
   "Latitudw": 105.3214598
 },
 {
   "STT": 2,
   "Name": "Trạm Y tế Phường Cái Khế",
   "address": "Phường Cái Khế, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.045236,
   "Latitudw": 105.780428
 },
 {
   "STT": 3,
   "Name": "Trạm Y tế Phường An Hòa",
   "address": "Phường An Hòa, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0395986,
   "Latitudw": 105.7663947
 },
 {
   "STT": 4,
   "Name": "Trạm Y tế Phường Thới Bình",
   "address": "Phường Thới Bình, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0414408,
   "Latitudw": 105.7709481
 },
 {
   "STT": 5,
   "Name": "Trạm Y tế Phường An Nghiệp",
   "address": "Phường An Nghiệp, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.039838,
   "Latitudw": 105.7707732
 },
 {
   "STT": 6,
   "Name": "Trạm Y tế Phường An Cư",
   "address": "Phường An Cư, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0357506,
   "Latitudw": 105.7815511
 },
 {
   "STT": 7,
   "Name": "Trạm Y tế Phường An Hội",
   "address": "Phường An Hội, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0401612,
   "Latitudw": 105.7831995
 },
 {
   "STT": 8,
   "Name": "Trạm Y tế Phường Tân An",
   "address": "Phường Tân An, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0332281,
   "Latitudw": 105.7825737
 },
 {
   "STT": 9,
   "Name": "Trạm Y tế Phường An Lạc",
   "address": "Phường An Lạc, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.025951,
   "Latitudw": 105.7803856
 },
 {
   "STT": 10,
   "Name": "Trạm Y tế Phường An Phú",
   "address": "Phường An Phú, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.4888251,
   "Latitudw": 105.7004553
 },
 {
   "STT": 11,
   "Name": "Trạm Y tế Phường Xuân Khánh",
   "address": "Phường Xuân Khánh, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0306107,
   "Latitudw": 105.7707825
 },
 {
   "STT": 12,
   "Name": "Trạm Y tế Phường Hưng Lợi",
   "address": "Phường Hưng Lợi, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0210745,
   "Latitudw": 105.767713
 },
 {
   "STT": 13,
   "Name": "Trạm Y tế Phường An Bình",
   "address": "Phường An Bình, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.009541,
   "Latitudw": 105.7484614
 },
 {
   "STT": 14,
   "Name": "Trạm Y tế Phường Châu Văn Liêm",
   "address": "Phường Châu Văn Liêm, Quận Ô Môn, Thành phố Cần Thơ",
   "Longtitude": 10.1162097,
   "Latitudw": 105.6218113
 },
 {
   "STT": 15,
   "Name": "Trạm Y tế Phường Thới Long",
   "address": "Phường Thới Long, Quận Ô Môn, Thành phố Cần Thơ",
   "Longtitude": 10.172495,
   "Latitudw": 105.5893605
 },
 {
   "STT": 16,
   "Name": "Trạm Y tế Phường Thới An",
   "address": "Phường Thới An, Quận Ô Môn, Thành phố Cần Thơ",
   "Longtitude": 10.1444991,
   "Latitudw": 105.6505458
 },
 {
   "STT": 17,
   "Name": "Trạm Y tế Phường Phước Thới",
   "address": "Phường Phước Thới, Quận Ô Môn, Thành phố Cần Thơ",
   "Longtitude": 10.1000169,
   "Latitudw": 105.6884765
 },
 {
   "STT": 18,
   "Name": "Trạm Y tế Phường Trường Lạc",
   "address": "Phường Trường Lạc, Quận Ô Môn, Thành phố Cần Thơ",
   "Longtitude": 10.0624314,
   "Latitudw": 105.6443706
 },
 {
   "STT": 19,
   "Name": "Trạm Y tế Phường Bình Thủy",
   "address": "Phường Bình Thủy, Quận Bình Thuỷ, Thành phố Cần Thơ",
   "Longtitude": 10.0753618,
   "Latitudw": 105.7473264
 },
 {
   "STT": 20,
   "Name": "Trạm Y tế Phường Trà Nóc",
   "address": "Phường Trà Nóc, Quận Bình Thuỷ, Thành phố Cần Thơ",
   "Longtitude": 10.0941407,
   "Latitudw": 105.7129115
 },
 {
   "STT": 21,
   "Name": "Trạm Y tế phường Thới An Đông",
   "address": "Phường Thới An Đông, Quận Bình Thuỷ, Thành phố Cần Thơ",
   "Longtitude": 10.0772432,
   "Latitudw": 105.6845122
 },
 {
   "STT": 22,
   "Name": "Trạm Y tế Phường An Thới",
   "address": "Phường An Thới, Quận Bình Thuỷ, Thành phố Cần Thơ",
   "Longtitude": 10.0609924,
   "Latitudw": 105.7616089
 },
 {
   "STT": 23,
   "Name": "Trạm Y tế Phường Long Hòa",
   "address": "Phường Long Hòa, Quận Bình Thuỷ, Thành phố Cần Thơ",
   "Longtitude": 10.063431,
   "Latitudw": 105.7244046
 },
 {
   "STT": 24,
   "Name": "Trạm Y tế Phường Long Tuyền",
   "address": "Phường Long Tuyền, Quận Bình Thuỷ, Thành phố Cần Thơ",
   "Longtitude": 10.0247976,
   "Latitudw": 105.7174266
 },
 {
   "STT": 25,
   "Name": "Trạm Y tế Phường Lê Bình",
   "address": "Phường Lê Bình, Quận Cái Răng, Thành phố Cần Thơ",
   "Longtitude": 10.004948,
   "Latitudw": 105.748783
 },
 {
   "STT": 26,
   "Name": "Trạm Y tế Phường Hưng Phú",
   "address": "Phường Hưng Phú, Quận Cái Răng, Thành phố Cần Thơ",
   "Longtitude": 10.0219949,
   "Latitudw": 105.7892136
 },
 {
   "STT": 27,
   "Name": "Trạm Y tế Phường Hưng Thạnh",
   "address": "Phường Hưng Thạnh, Quận Cái Răng, Thành phố Cần Thơ",
   "Longtitude": 9.9949768,
   "Latitudw": 105.7740093
 },
 {
   "STT": 28,
   "Name": "Trạm Y tế Phường Ba Láng",
   "address": "Phường Ba Láng, Quận Cái Răng, Thành phố Cần Thơ",
   "Longtitude": 9.9841151,
   "Latitudw": 105.7408815
 },
 {
   "STT": 29,
   "Name": "Trạm Y tế Phường Thường Thạnh",
   "address": "Phường Thường Thạnh, Quận Cái Răng, Thành phố Cần Thơ",
   "Longtitude": 9.977242,
   "Latitudw": 105.7406588
 },
 {
   "STT": 30,
   "Name": "Trạm Y tế Phường Phú Thứ",
   "address": "Phường Phú Thứ, Quận Cái Răng, Thành phố Cần Thơ",
   "Longtitude": 9.9939196,
   "Latitudw": 105.7646572
 },
 {
   "STT": 31,
   "Name": "Trạm Y tế Phường Tân Phú",
   "address": "Phường Tân Phú, Quận Cái Răng, Thành phố Cần Thơ",
   "Longtitude": 9.9818746,
   "Latitudw": 105.8209701
 },
 {
   "STT": 32,
   "Name": "Trạm Y tế Phường Thốt Nốt",
   "address": "Thị trấn Thốt Nốt, Huyện Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.2680833,
   "Latitudw": 105.527661
 },
 {
   "STT": 33,
   "Name": "Trạm Y tế Phường Thới Thuận",
   "address": "Xã Thới Thuận, Huyện Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.2904039,
   "Latitudw": 105.50422
 },
 {
   "STT": 34,
   "Name": "Trạm Y tế Phường Tân Lộc",
   "address": "Xã Tân Lộc, Huyện Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.2479032,
   "Latitudw": 105.581909
 },
 {
   "STT": 35,
   "Name": "Trạm Y tế Phường Trung Nhất",
   "address": "Xã Trung Nhất, Huyện Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.247991,
   "Latitudw": 105.549078
 },
 {
   "STT": 36,
   "Name": "Trạm Y tế Phường Trung Kiên",
   "address": "Xã Trung Kiên, Huyện Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.2445995,
   "Latitudw": 105.5511106
 },
 {
   "STT": 37,
   "Name": "Trạm Y tế Xã Trung An",
   "address": "Xã Trung An, Huyện Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.2148192,
   "Latitudw": 105.4924993
 },
 {
   "STT": 38,
   "Name": "Trạm Y tế Xã Trung Thạnh",
   "address": "Xã Trung Thạnh, Huyện Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.2092882,
   "Latitudw": 105.5147086
 },
 {
   "STT": 39,
   "Name": "Trạm Y tế Phường Thuận Hưng",
   "address": "Xã Thuận Hưng, Huyện Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.2005381,
   "Latitudw": 105.5628352
 },
 {
   "STT": 40,
   "Name": "Trạm Y tế Thị trấn Thạnh An",
   "address": "Thị trấn Thanh An, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.3141154,
   "Latitudw": 105.4810302
 },
 {
   "STT": 41,
   "Name": "Trạm Y tế xã Thạnh Mỹ",
   "address": "Xã Thạnh Mỹ, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.2481438,
   "Latitudw": 105.4107928
 },
 {
   "STT": 42,
   "Name": "Trạm Y tế xã Vĩnh Trinh",
   "address": "Xã Vĩnh Trinh, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.2758037,
   "Latitudw": 105.44218
 },
 {
   "STT": 43,
   "Name": "Trạm Y tế xã Thạnh An",
   "address": "Xã Thạnh An, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.2228845,
   "Latitudw": 105.3139962
 },
 {
   "STT": 44,
   "Name": "Trạm Y tế xã Thạnh Thắng",
   "address": "Xã Thạnh Thắng, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.1980229,
   "Latitudw": 105.2758096
 },
 {
   "STT": 45,
   "Name": "Trạm Y tế xã Thạnh Qưới",
   "address": "Xã Thạnh Qưới, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.1843531,
   "Latitudw": 105.3343473
 },
 {
   "STT": 46,
   "Name": "Trạm Y tế xã Thạnh Phú",
   "address": "Xã Thạnh Phú, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.1363868,
   "Latitudw": 105.4280496
 },
 {
   "STT": 47,
   "Name": "Trạm Y tế xã Thạnh Lộc",
   "address": "Xã Thạnh Lộc, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.2049933,
   "Latitudw": 105.4341504
 },
 {
   "STT": 48,
   "Name": "Trạm Y tế Xã Trung Hưng",
   "address": "Xã Trung Hưng, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.18665,
   "Latitudw": 105.4806214
 },
 {
   "STT": 49,
   "Name": "Trạm Y tế Thị trấn Thới Lai",
   "address": "Thị trấn Thới Lai, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0593487,
   "Latitudw": 105.5628352
 },
 {
   "STT": 50,
   "Name": "Trạm Y tế thị trấn Cờ Đỏ",
   "address": "Thị trấn Cờ Đỏ, Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.0942327,
   "Latitudw": 105.4166934
 },
 {
   "STT": 51,
   "Name": "Trạm Y tế xã Thới Hưng",
   "address": "Xã Thới Hưng, Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.1227748,
   "Latitudw": 105.5243767
 },
 {
   "STT": 52,
   "Name": "Trạm Y tế xã Thới Thạnh",
   "address": "Xã Thới Thạnh, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.089602,
   "Latitudw": 105.5924588
 },
 {
   "STT": 53,
   "Name": "Trạm Y tế xã Xuân Thắng",
   "address": "Xã Xuân Thắng, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0821796,
   "Latitudw": 105.5393868
 },
 {
   "STT": 54,
   "Name": "Trạm Y tế xã Đông Hiệp",
   "address": "Xã Đông Hiệp, Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.0730067,
   "Latitudw": 105.50422
 },
 {
   "STT": 55,
   "Name": "Trạm Y tế xã Thới Đông",
   "address": "Xã Thới Đông, Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.056309,
   "Latitudw": 105.3987621
 },
 {
   "STT": 56,
   "Name": "Trạm Y tế xã Đông Bình",
   "address": "Xã Đông Bình, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0017382,
   "Latitudw": 105.4432248
 },
 {
   "STT": 57,
   "Name": "Trạm Y tế xã Đông Thuận",
   "address": "Xã Đông Thuận, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0307085,
   "Latitudw": 105.4807794
 },
 {
   "STT": 58,
   "Name": "Trạm Y tế xã Thới Lai",
   "address": "Xã Thới Lai, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0644445,
   "Latitudw": 105.5372427
 },
 {
   "STT": 59,
   "Name": "Trạm Y tế xã Định Môn",
   "address": "Xã Định Môn, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0564275,
   "Latitudw": 105.6089437
 },
 {
   "STT": 60,
   "Name": "Trạm Y tế xã Trường Thành",
   "address": "Xã Trường Thành, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0256791,
   "Latitudw": 105.5862867
 },
 {
   "STT": 61,
   "Name": "Trạm Y tế xã Trường Xuân",
   "address": "Xã Trường Xuân, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 9.9826522,
   "Latitudw": 105.5144045
 },
 {
   "STT": 62,
   "Name": "Trạm Y tế xã Trường Xuân A",
   "address": "Xã Trường Xuân A, Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 9.9902357,
   "Latitudw": 105.5335252
 },
 {
   "STT": 63,
   "Name": "Trạm Y tế xã Nhơn ái",
   "address": "Xã Nhơn ái, Huyện Phong Điền, Thành phố Cần Thơ",
   "Longtitude": 9.9889683,
   "Latitudw": 105.6630754
 },
 {
   "STT": 64,
   "Name": "Trạm Y tế xã Giai Xuân",
   "address": "Xã Giai Xuân, Huyện Phong Điền, Thành phố Cần Thơ",
   "Longtitude": 10.0275079,
   "Latitudw": 105.6855235
 },
 {
   "STT": 65,
   "Name": "Trạm Y tế xã Tân Thới",
   "address": "Xã Tân Thới, Huyện Phong Điền, Thành phố Cần Thơ",
   "Longtitude": 10.0160246,
   "Latitudw": 105.6473248
 },
 {
   "STT": 66,
   "Name": "Trạm Y tế xã Trường Long",
   "address": "Xã Trường Long, Huyện Phong Điền, Thành phố Cần Thơ",
   "Longtitude": 10.0064019,
   "Latitudw": 105.6179633
 },
 {
   "STT": 67,
   "Name": "Trạm Y tế xã Mỹ Khánh",
   "address": "Xã Mỹ Khánh, Huyện Phong Điền, Thành phố Cần Thơ",
   "Longtitude": 9.99837,
   "Latitudw": 105.7035905
 },
 {
   "STT": 68,
   "Name": "Trạm Y tế xã Nhơn Nghĩa",
   "address": "Xã Nhơn Nghĩa, Huyện Phong Điền, Thành phố Cần Thơ",
   "Longtitude": 9.9851473,
   "Latitudw": 105.688213
 },
 {
   "STT": 69,
   "Name": "Trạm Y tế Xã Trung Thành",
   "address": "Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.2092882,
   "Latitudw": 105.5147086
 },
 {
   "STT": 70,
   "Name": "Trạm Y tế thị trấn Vĩnh Thạnh",
   "address": "Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.2245455,
   "Latitudw": 105.3951076
 },
 {
   "STT": 71,
   "Name": "Trạm Y tế Phường An Khánh",
   "address": "Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0384302,
   "Latitudw": 105.7516959
 },
 {
   "STT": 72,
   "Name": "Trạm Y tế Phường Bùi Hữu Nghĩa",
   "address": "Quận Bình Thủy, Thành phố Cần Thơ",
   "Longtitude": 10.0599618,
   "Latitudw": 105.7643428
 },
 {
   "STT": 73,
   "Name": "Trạm Y tế Phường Trà An",
   "address": "Quận Bình Thủy, Thành phố Cần Thơ",
   "Longtitude": 10.0820389,
   "Latitudw": 105.7251691
 },
 {
   "STT": 74,
   "Name": "Trạm Y tế Phường Long Hưng",
   "address": "Quận Ô Môn, Thành phố Cần Thơ",
   "Longtitude": 10.1546125,
   "Latitudw": 105.5751633
 },
 {
   "STT": 75,
   "Name": "Trạm Y tế Phường Thới Hoà",
   "address": "Quận Ô Môn, Thành phố Cần Thơ",
   "Longtitude": 10.1158008,
   "Latitudw": 105.6274009
 },
 {
   "STT": 76,
   "Name": "Trạm Y tế thị trấn Phong Điền",
   "address": "Huyện Phong Điền, Thành phố Cần Thơ",
   "Longtitude": 10.0064019,
   "Latitudw": 105.6179633
 },
 {
   "STT": 77,
   "Name": "Trạm Y tế xã Thới Tân",
   "address": "Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0160246,
   "Latitudw": 105.6473248
 },
 {
   "STT": 78,
   "Name": "Trạm Y tế xã Trường Xuân B",
   "address": "Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 9.9826259,
   "Latitudw": 105.5209706
 },
 {
   "STT": 79,
   "Name": "Trạm Y tế xã Trường Thắng",
   "address": "Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0247536,
   "Latitudw": 105.3814023
 },
 {
   "STT": 80,
   "Name": "Trạm Y tế xã Tân thạnh",
   "address": "Huyện Thới Lai, Thành phố Cần Thơ",
   "Longtitude": 10.0885579,
   "Latitudw": 105.5835479
 },
 {
   "STT": 81,
   "Name": "Trạm Y tế xã Thới Xuân",
   "address": "Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.0774723,
   "Latitudw": 105.4104765
 },
 {
   "STT": 82,
   "Name": "Trạm Y tế xã Đông Thắng",
   "address": "Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.0858043,
   "Latitudw": 105.4698843
 },
 {
   "STT": 83,
   "Name": "Trạm Y tế Phường Tân Hưng",
   "address": "Quận Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.1877587,
   "Latitudw": 105.5485096
 },
 {
   "STT": 84,
   "Name": "Trạm Y tế Phường Thạnh Hoà",
   "address": "Quận Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.2418905,
   "Latitudw": 105.5325228
 },
 {
   "STT": 85,
   "Name": "Trạm Y tế Phường Thuận An",
   "address": "Quận Thốt Nốt, Thành phố Cần Thơ",
   "Longtitude": 10.2418905,
   "Latitudw": 105.5325228
 },
 {
   "STT": 86,
   "Name": "Trạm Y tế xã Thạnh Tiến",
   "address": "Xã Thạnh Tiến, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.1856098,
   "Latitudw": 105.3496921
 },
 {
   "STT": 87,
   "Name": "Trạm Y tế Thị trấn Vĩnh Thạnh",
   "address": "Thị trấn Vĩnh Thạnh, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.1843531,
   "Latitudw": 105.3343473
 },
 {
   "STT": 88,
   "Name": "Trạm Y tế xã Vĩnh Bình",
   "address": "Xã Vĩnh Bình, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.2574177,
   "Latitudw": 105.4334721
 },
 {
   "STT": 89,
   "Name": "Trạm Y tế xã Thạnh Lợi",
   "address": "Xã Thạnh Lợi, Huyện Vĩnh Thạnh, Thành phố Cần Thơ",
   "Longtitude": 10.2250058,
   "Latitudw": 105.240612
 },
 {
   "STT": 90,
   "Name": "Trạm Y tế Thị trấn Cờ Đỏ",
   "address": "Số 01 Ấp Thới Hòa B, Thị trấn Cờ Đỏ, Huyện Cờ Đỏ, Thành phố Cần Thơ",
   "Longtitude": 10.102475,
   "Latitudw": 105.4281008
 }
];