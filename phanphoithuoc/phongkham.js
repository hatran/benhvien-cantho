var dataphongkham = [
 {
   "STT": 1,
   "Name": "Phòng khám Y Khoa Vạn Phước Cửu Long",
   "address": "Số 19 Đường Nguyễn Văn Linh, Phường Hưng Lợi, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0222717,
   "Latitude": 105.7637933
 },
 {
   "STT": 2,
   "Name": "Phòng khám đa khoa tư nhân Hoàn Hảo",
   "address": "307C Nguyễn Văn Linh, Phường An Khánh, Quận Ninh kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0296536,
   "Latitude": 105.7538002
 },
 {
   "STT": 3,
   "Name": "Phòng khám đa khoa tư nhân Âu Cơ",
   "address": "Trần Ngọc Quế, Phường Hưng Lợi, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0232444,
   "Latitude": 105.7694825
 },
 {
   "STT": 4,
   "Name": "Phòng khám đa khoa tư nhân Minh Tâm",
   "address": "Số 21 đường 30/4, Phường An Lạc, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0232569,
   "Latitude": 105.771531
 },
 {
   "STT": 5,
   "Name": "Phòng khám đa khoa tư nhân Trọng Nghĩa",
   "address": "Số 156 Nguyễn An Ninh, Phường Tân An, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.031562,
   "Latitude": 105.7833917
 },
 {
   "STT": 6,
   "Name": "Phòng khám đa khoa Quốc tế Phương Châu",
   "address": "300 Nguyễn Văn Cừ , Phường An Khánh, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0300798,
   "Latitude": 105.7513582
 },
 {
   "STT": 7,
   "Name": "Phòng khám đa khoa tư nhân Phước Hải",
   "address": "140 Nguyễn An Ninh, Phường Tân An, Quận Ninh Kiều, Thành phốCần Thơ",
   "Longtitude": 10.031611,
   "Latitude": 105.784016
 },
 {
   "STT": 8,
   "Name": "Phòng khám đa khoa tư nhân Gia Phước",
   "address": "57 Hùng Vương, Phường Thới Bình, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.043961,
   "Latitude": 105.778756
 },
 {
   "STT": 9,
   "Name": "Phòng khám đa khoa tư nhân Nguyễn Thái Học",
   "address": "102 Nguyễn Thái Học, Phường Tân An, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.03299,
   "Latitude": 105.784338
 },
 {
   "STT": 10,
   "Name": "Phòng khám đa khoa tư nhân Ngọc Thạch",
   "address": " Phường Trà An, Quận Bình Thủy, Thành phố Cần Thơ",
   "Longtitude": 10.0873962,
   "Latitude": 105.7145801
 },
 {
   "STT": 11,
   "Name": "Phòng khám Chấn thương chỉnh hình - Bác sĩ Phạm Việt Triều",
   "address": "5A Mậu Thân,Phường Xuân Khánh, Quận Ninh Kiều,Thành phố Cần Thơ",
   "Longtitude": 10.032621,
   "Latitude": 105.7744869
 },
 {
   "STT": 12,
   "Name": "Phòng khám Bác sĩ Năng",
   "address": "190 Đường 30/4,Phường Hưng Lợi, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.0102683,
   "Latitude": 105.7605058
 },
 {
   "STT": 13,
   "Name": "Phòng khám Ngoại - Bác sĩ Đàm Văn Cương",
   "address": "6A Cách mạng Tháng 8,Phường Cái Khế, Quận Ninh Kiều, Thành phố Cần Thơ",
   "Longtitude": 10.04597,
   "Latitude": 105.779623
 },
 {
   "STT": 14,
   "Name": "Phòng khám Bác sĩ Giang",
   "address": "40A đường 3/2, Hưng Lợi, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.019693,
   "Latitude": 105.7576547
 },
 {
   "STT": 15,
   "Name": "Phòng khám Ngoại tổng hợp - Bác sĩ Nguyễn Quang Tiến",
   "address": "220A/C1/D6 Nguyễn Văn Cừ,Phường An Hòa, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.046805,
   "Latitude": 105.768563
 },
 {
   "STT": 16,
   "Name": "Phòng khám Ngoại tổng hợp - Bác sĩ Nguyễn Trung Hiếu",
   "address": "32 Trần Bạch Đằng, Phường An Khánh, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.0366462,
   "Latitude": 105.7546229
 },
 {
   "STT": 17,
   "Name": "Phòng khám Ngoại tổng hợp - Bác sĩ Lê Quang Trung",
   "address": "25 Xô Viết Nghệ Tĩnh,Phường An Cư, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.0382232,
   "Latitude": 105.7839009
 },
 {
   "STT": 18,
   "Name": "Phòng khám - Bác sĩ Trần Huỳnh Tuấn",
   "address": "306B4 Nguyễn Văn Linh, Phường An Khánh, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.0303881,
   "Latitude": 105.752001
 },
 {
   "STT": 19,
   "Name": "Phòng khám Ngoại tổng hợp - Bác sĩ Nguyễn Văn Tống",
   "address": "56 Mậu Thân, Phường Xuân Khánh, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.03783,
   "Latitude": 105.771161
 },
 {
   "STT": 20,
   "Name": "Phòng khám Bác sĩ Lê Dũng",
   "address": "337A quốc lộ 91B, Phường  An Khánh,Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.0331756,
   "Latitude": 105.7489343
 },
 {
   "STT": 21,
   "Name": "Phòng khám - Bác sĩ Nguyễn Văn Tuấn",
   "address": "2/51E Lê Lai, Phường An Phú, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.0331582,
   "Latitude": 105.7798041
 },
 {
   "STT": 22,
   "Name": "Phòng khám Đa Khoa Gia Phước",
   "address": "57 Hùng Vương,Phường An Hội, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.043961,
   "Latitude": 105.778756
 },
 {
   "STT": 23,
   "Name": "Phòng khám Tai Mũi Họng - Bác sĩ Nguyễn Triều Việt",
   "address": "188 Ba Tháng Hai, Phường Hưng Lợi, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.020416,
   "Latitude": 105.763929
 },
 {
   "STT": 24,
   "Name": "Phòng khám Đa Khoa Thế Kỷ Mới ",
   "address": "133A Trần Hưng Đạo, Phường An Phú, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.034251,
   "Latitude": 105.775336
 },
 {
   "STT": 25,
   "Name": "Phòng khám Bác Sĩ Đoàn Anh Vũ-khám Bệnh Trĩ Tiêu Hoá Gan Mật",
   "address": " 351AA đường Nguyễn Văn Cừ nối dài, Phường An Bình, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.0218627,
   "Latitude": 105.7432882
 },
 {
   "STT": 26,
   "Name": "Phòng khám Chuyên Khoa Siêu Âm - Bs Thái Thị Kim Nguyên",
   "address": "20 30 tháng 4, Phường An Lạc, Quận Ninh Kiều, Thành phố Cần Thơ ",
   "Longtitude": 10.0102683,
   "Latitude": 105.7605058
 }
];