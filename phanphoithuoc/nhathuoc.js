var datanhathuoc = [
 {
   "STT": 1,
   "Name": "Nhà thuốc Quân khu 9",
   "address": "28 Huỳnh Phan Hộ, Trà Nóc, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0786962,
   "Latitude": 105.7268254
 },
 {
   "STT": 2,
   "Name": "Nhà thuốc Phước Thịnh 2",
   "address": "77 Nguyễn Chí Thanh, Trà Nóc, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0959621,
   "Latitude": 105.7112368
 },
 {
   "STT": 3,
   "Name": "Nhà thuốc Minh Thi 2",
   "address": "Nguyễn Hiền, khu dân cư 91B, An Khánh, Ninh Kiều,Thành Phố Cần Thơ ",
   "Longtitude": 10.0245879,
   "Latitude": 105.7552853
 },
 {
   "STT": 4,
   "Name": "Nhà thuốc Nguyên Vũ",
   "address": "An Bình, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0119306,
   "Latitude": 105.7514827
 },
 {
   "STT": 5,
   "Name": "Nhà thuốc Tân Trào",
   "address": "An Hòa, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0497364,
   "Latitude": 105.7759348
 },
 {
   "STT": 6,
   "Name": "Nhà thuốc Thanh Thảo",
   "address": "Trần Việt Châu, An Hòa, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.033376,
   "Latitude": 105.7851487
 },
 {
   "STT": 7,
   "Name": "Nhà thuốc Phú Khang",
   "address": "An Lạc, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0304762,
   "Latitude": 105.7785625
 },
 {
   "STT": 8,
   "Name": "Nhà thuốc Y học cổ truyền Hiệp Hòa",
   "address": "Lê Thị Tạo, Thốt Nốt, Thốt Nốt,Thành Phố Cần Thơ",
   "Longtitude": 10.2713135,
   "Latitude": 105.531842
 },
 {
   "STT": 9,
   "Name": "Nhà thuốc Bảo Trâm",
   "address": "Lê Hồng Phong, Trà Nóc, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0985011,
   "Latitude": 105.7154595
 },
 {
   "STT": 10,
   "Name": "Nhà thuốc Nhi Nguyên",
   "address": "Nguyễn Chí Thanh, Trà Nóc, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0935279,
   "Latitude": 105.7092874
 },
 {
   "STT": 11,
   "Name": "Nhà thuốc Hoàng Yến",
   "address": "Ngô Quyền, Tân An, Ninh Kiều,Thành Phố Cần Thơ ",
   "Longtitude": 10.0344906,
   "Latitude": 105.7844889
 },
 {
   "STT": 12,
   "Name": "Nhà thuốc Thảo Trang",
   "address": "Nguyễn Văn Cừ, An Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0305109,
   "Latitude": 105.7461276
 },
 {
   "STT": 13,
   "Name": "Nhà thuốc An Lạc",
   "address": " Nguyễn Thị Minh Khai, An Lạc, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0262465,
   "Latitude": 105.7809441
 },
 {
   "STT": 15,
   "Name": "Nhà thuốc Tuyết Anh",
   "address": "Hai Bà Trưng, Tân An, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0274981,
   "Latitude": 105.7843913
 },
 {
   "STT": 16,
   "Name": "Nhà thuốc Châu An ",
   "address": "Hai Bà Trưng, Tân An, Ninh Kiều,Thành Phố Cần Thơ ",
   "Longtitude": 10.029148,
   "Latitude": 105.785288
 },
 {
   "STT": 17,
   "Name": "Nhà thuốc Phương Đông",
   "address": "đường 30 Tháng 4, An Phú, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0280777,
   "Latitude": 105.7759004
 },
 {
   "STT": 18,
   "Name": "Nhà thuốc Imexpharm",
   "address": " Hùng Vương, Thới Bình, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0435697,
   "Latitude": 105.7763097
 },
 {
   "STT": 19,
   "Name": "Nhà thuốc Lê Hoàng",
   "address": " quốc lộ 1, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 9.9933171,
   "Latitude": 105.7447208
 },
 {
   "STT": 20,
   "Name": "Nhà thuốc Hoàn Huy 2",
   "address": "quốc lộ 1, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.0025677,
   "Latitude": 105.7484132
 },
 {
   "STT": 21,
   "Name": "Nhà thuốc Cát Tường ",
   "address": "Trần Hưng Đạo, Lê Bình, Cái Răng,Thành Phố Cần Thơ ",
   "Longtitude": 9.9980617,
   "Latitude": 105.7537659
 },
 {
   "STT": 22,
   "Name": "Nhà thuốc Thiên Kim",
   "address": "Quốc lộ 91B, Hưng Lợi, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0228844,
   "Latitude": 105.7601482
 },
 {
   "STT": 23,
   "Name": "Nhà thuốc Phương Chi",
   "address": "Tầm Vu , Hưng Lợi, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0140881,
   "Latitude": 105.7691536
 },
 {
   "STT": 24,
   "Name": "Nhà thuốc Ninh Kiều ",
   "address": "Nguyễn Thị Minh Khai, An Lạc, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0264683,
   "Latitude": 105.7840126
 },
 {
   "STT": 25,
   "Name": "Nhà thuốc Dược Hậu Giang",
   "address": "Bis Nguyễn Văn Cừ, An Hòa, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0465052,
   "Latitude": 105.7662165
 },
 {
   "STT": 26,
   "Name": "Nhà thuốc Phương Thảo ",
   "address": " Trần Việt Châu, An Hòa, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0500945,
   "Latitude": 105.7709971
 },
 {
   "STT": 27,
   "Name": "Nhà thuốc Mỹ Ngân ",
   "address": "Võ Tánh, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.0050348,
   "Latitude": 105.746895
 },
 {
   "STT": 28,
   "Name": "Nhà thuốc Thành Đạt",
   "address": "khu dân cư Thới Nhựt, An Khánh, Ninh Kiều,Thành Phố Cần Thơ ",
   "Longtitude": 10.0393368,
   "Latitude": 105.7520802
 },
 {
   "STT": 29,
   "Name": "Nhà thuốc Huỳnh Lộc",
   "address": "Hùng Vương, Thới Bình, Ninh Kiều,Thành Phố Cần Thơ ",
   "Longtitude": 10.0445196,
   "Latitude": 105.7773173
 },
 {
   "STT": 30,
   "Name": "Nhà thuốc Anh Thư ",
   "address": "Mậu Thân, Xuân Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0293766,
   "Latitude": 105.7742
 },
 {
   "STT": 31,
   "Name": "Nhà thuốc Lan Hương",
   "address": "152 Ba Tháng Hai, Xuân Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0260598,
   "Latitude": 105.7668024
 },
 {
   "STT": 32,
   "Name": "Nhà thuốc bệnh viện Thanh Quang",
   "address": " Nguyễn Thị Minh Khai, Xuân Khánh, Ninh Kiều,Thành Phố Cần Thơ ",
   "Longtitude": 10.0259295,
   "Latitude": 105.7767384
 },
 {
   "STT": 33,
   "Name": "Nhà thuốc Ngọc Lan ",
   "address": "Trần Hoàng Na, Hưng Lợi, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.013096,
   "Latitude": 105.7615234
 },
 {
   "STT": 34,
   "Name": "Nhà thuốc Thùy Linh ",
   "address": "đường 30/4, Hưng Lợi, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0116119,
   "Latitude": 105.7571572
 },
 {
   "STT": 35,
   "Name": "Nhà thuốc Thiện Nhân ",
   "address": "Ba Tháng Hai, Xuân Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0285496,
   "Latitude": 105.7520687
 },
 {
   "STT": 36,
   "Name": "Nhà thuốc Lợi Nhân",
   "address": "đường 3/2, Xuân Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0290814,
   "Latitude": 105.7693612
 },
 {
   "STT": 37,
   "Name": "Nhà thuốc Bích Ngọc",
   "address": "Nguyễn Thái Học, Thốt Nốt, Thốt Nốt,Thành Phố Cần Thơ",
   "Longtitude": 10.270524,
   "Latitude": 105.5304515
 },
 {
   "STT": 38,
   "Name": "Nhà thuốc Minh Thư",
   "address": " Lê Hồng Phong, Trà Nóc, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.1011207,
   "Latitude": 105.7011526
 },
 {
   "STT": 39,
   "Name": "Nhà thuốc Bình An",
   "address": "Nguyễn Trãi, Cái Khế, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.044342,
   "Latitude": 105.7790011
 },
 {
   "STT": 40,
   "Name": "Nhà thuốc Phú An",
   "address": "khu dân cư 586, Phú Thứ, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 9.996945,
   "Latitude": 105.7133229
 },
 {
   "STT": 41,
   "Name": "Nhà thuốc Nguyễn Khánh",
   "address": "quốc lộ 1A, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 9.9973587,
   "Latitude": 105.747469
 },
 {
   "STT": 42,
   "Name": "Nhà thuốc Mai Xuân ",
   "address": "Trần Quang Diệu, An Thới, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0586459,
   "Latitude": 105.7602256
 },
 {
   "STT": 43,
   "Name": "Nhà thuốc Nguyễn Thúy Vân",
   "address": "Lê Hồng Phong, Bình Thủy, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0804559,
   "Latitude": 105.7397961
 },
 {
   "STT": 44,
   "Name": "Nhà thuốc Bình Tân 3",
   "address": "Thạnh Hưng, Phú Thứ, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 9.9939196,
   "Latitude": 105.7646572
 },
 {
   "STT": 45,
   "Name": "Nhà thuốc 174",
   "address": "174 Trần Quang Diệu, An Thới, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0540136,
   "Latitude": 105.7515836
 },
 {
   "STT": 46,
   "Name": "Nhà thuốc Minh Thúy ",
   "address": "2/4 Lê Thái Tổ, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.0049724,
   "Latitude": 105.7490535
 },
 {
   "STT": 47,
   "Name": "Nhà thuốc Đức Mạnh",
   "address": "261A/10 Nguyễn Văn Cừ, An Bình, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0470246,
   "Latitude": 105.7657557
 },
 {
   "STT": 48,
   "Name": "Nhà thuốc Tân Nga ",
   "address": "Xô Viết Nghệ Tĩnh , An Cư, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0377568,
   "Latitude": 105.782103
 },
 {
   "STT": 49,
   "Name": "Nhà thuốc Phương Huỳnh",
   "address": "161 Trần Hưng Đạo, An Phú, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0434464,
   "Latitude": 105.7633467
 },
 {
   "STT": 50,
   "Name": "Nhà thuốc Ngân Yến ",
   "address": "92 Nguyễn Thị Minh Khai, An Lạc, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0261831,
   "Latitude": 105.7809441
 },
 {
   "STT": 51,
   "Name": "Nhà thuốc Thiên Tân",
   "address": "214 Ba Tháng Hai, Hưng Lợi, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0222353,
   "Latitude": 105.7631546
 },
 {
   "STT": 52,
   "Name": "Nhà thuốc Minh Thi",
   "address": "406 Nguyễn Văn Linh, An Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0245879,
   "Latitude": 105.7552853
 },
 {
   "STT": 53,
   "Name": "Nhà thuốc Bệnh viện ",
   "address": "6 đường 30/4, Hưng Lợi, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0124694,
   "Latitude": 105.7548516
 },
 {
   "STT": 54,
   "Name": "Nhà thuốc Hồng Trang",
   "address": "21 Đề Thám, An Cư, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0336637,
   "Latitude": 105.780898
 },
 {
   "STT": 55,
   "Name": "Nhà thuốc Như Ngọc",
   "address": "58 vành đai Phi Trường, An Thới, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0489217,
   "Latitude": 105.7616311
 },
 {
   "STT": 56,
   "Name": "Nhà thuốc Khỏe 2",
   "address": "36 Trần Việt Châu, An Hòa, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0506432,
   "Latitude": 105.770702
 },
 {
   "STT": 57,
   "Name": "Nhà thuốc Như Lan",
   "address": "114 Phan Đình Phùng, An Lạc, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0295163,
   "Latitude": 105.7841788
 },
 {
   "STT": 58,
   "Name": "Nhà thuốc Sơn Thủy",
   "address": "1 Trần Hưng Đạo, An Cư, Ninh Kiều,Thành Phố Cần Thơ ",
   "Longtitude": 10.0401121,
   "Latitude": 105.7752424
 },
 {
   "STT": 59,
   "Name": "Nhà thuốc dược sĩ Đoàn Kiều Vân Bích",
   "address": "433 quốc lộ 91 , Thới Thuận, Thốt Nốt,Thành Phố Cần Thơ ",
   "Longtitude": 10.3067315,
   "Latitude": 105.5010607
 },
 {
   "STT": 60,
   "Name": "Nhà thuốc Công Thành",
   "address": "39 Ngô Văn Sở, Tân An, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0339103,
   "Latitude": 105.7832823
 },
 {
   "STT": 61,
   "Name": "Nhà thuốc Vân Phương",
   "address": "46A/13 Lê Hồng Phong, Bình Thủy, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0858126,
   "Latitude": 105.7307482
 },
 {
   "STT": 62,
   "Name": "Nhà thuốc Trà An",
   "address": "4/19 Lê Hồng Phong, Bình Thủy, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0854344,
   "Latitude": 105.7325927
 },
 {
   "STT": 63,
   "Name": "Nhà thuốc Hồng Gấm",
   "address": "288 quốc lộ 91B, Long Hòa, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0554133,
   "Latitude": 105.7753913
 },
 {
   "STT": 64,
   "Name": "Nhà thuốc Nguyễn Kha",
   "address": "165/4 khu vực Yên Hòa, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 9.9973587,
   "Latitude": 105.747469
 },
 {
   "STT": 65,
   "Name": "Nhà thuốc Mỹ Ngọc",
   "address": "79 Nguyễn Trãi, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.0037706,
   "Latitude": 105.7496928
 },
 {
   "STT": 66,
   "Name": "Nhà thuốc Nguyễn Thị Phương Thanh",
   "address": "5/1 Ấp Mỹ Phước, Mỹ Khánh, Phong Điền,Thành Phố Cần Thơ",
   "Longtitude": 9.9983457,
   "Latitude": 105.7190099
 },
 {
   "STT": 67,
   "Name": "Nhà thuốc Hoàng Châu",
   "address": "48 Vành Đai Phi Trường, An Thới, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0491329,
   "Latitude": 105.7620763
 },
 {
   "STT": 68,
   "Name": "Nhà thuốc Phú Lộc",
   "address": "155 Nguyễn Văn Cừ, An Hòa, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0491385,
   "Latitude": 105.7688405
 },
 {
   "STT": 69,
   "Name": "Nhà thuốc Hồng Thu",
   "address": "160 Cách Mạng Tháng Tám, Cái Khế, Ninh Kiều,Thành Phố Cần Thơ ",
   "Longtitude": 10.0517427,
   "Latitude": 105.7717669
 },
 {
   "STT": 70,
   "Name": "Nhà thuốc Ngọc Ánh",
   "address": "178 Xô Viết Nghệ Tĩnh, An Hội, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0405652,
   "Latitude": 105.7796448
 },
 {
   "STT": 71,
   "Name": "Nhà thuốc Kim Ngân",
   "address": "455 Nguyễn Trãi, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.0029393,
   "Latitude": 105.7491224
 },
 {
   "STT": 72,
   "Name": "Nhà thuốc Lăng Thị Ngọc Bích",
   "address": "Ấp Mỹ Phước, Mỹ Khánh, Phong Điền,Thành Phố Cần Thơ",
   "Longtitude": 10.0031536,
   "Latitude": 105.7101003
 },
 {
   "STT": 73,
   "Name": "Nhà thuốc Kiều Trang",
   "address": "528 quốc lộ 91 , Thới Thuận, Thốt Nốt,Thành Phố Cần Thơ",
   "Longtitude": 10.3096278,
   "Latitude": 105.4910927
 },
 {
   "STT": 74,
   "Name": "Nhà thuốc Hoàn Huy 1",
   "address": "27/03 Ngô Quyền, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.0051125,
   "Latitude": 105.7496238
 },
 {
   "STT": 75,
   "Name": "Nhà thuốc Minh Chi ",
   "address": "391/1C quốc lộ 91B, An Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0217824,
   "Latitude": 105.7619995
 },
 {
   "STT": 76,
   "Name": "Nhà thuốc Nhật Tân",
   "address": "12 đường 30 tháng 4, An Phú, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0310043,
   "Latitude": 105.7791512
 },
 {
   "STT": 77,
   "Name": "Nhà thuốc Trí Nhân",
   "address": "16 Nguyễn An Ninh, Tân An, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0348498,
   "Latitude": 105.7868009
 },
 {
   "STT": 78,
   "Name": "Nhà thuốc Hồng Hạnh",
   "address": "1/17 Lê Hồng Phong, Bình Thủy, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0854983,
   "Latitude": 105.7325023
 },
 {
   "STT": 79,
   "Name": "Nhà thuốc Trường Thọ",
   "address": "8A/9 Đinh Công Tráng, Xuân Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0356807,
   "Latitude": 105.7815833
 },
 {
   "STT": 80,
   "Name": "Nhà thuốc Khánh Khoa",
   "address": "79/5 Nguyễn Trãi, Lê Bình , Cái Răng,Thành Phố Cần Thơ ",
   "Longtitude": 10.0038673,
   "Latitude": 105.7498937
 },
 {
   "STT": 81,
   "Name": "Nhà thuốc Thanh Tiền ",
   "address": "4/D6 đường số 14, Phú Thứ, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.0042952,
   "Latitude": 105.8049488
 },
 {
   "STT": 82,
   "Name": "Nhà thuốc Ngọc Duệ",
   "address": "9/1 Lê Thái Tổ, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.0049588,
   "Latitude": 105.7490583
 },
 {
   "STT": 83,
   "Name": "Nhà thuốc Tú Hoa",
   "address": "55/5 Duy Tân, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.0055039,
   "Latitude": 105.7491195
 },
 {
   "STT": 84,
   "Name": "Nhà thuốc Ngọc Bích",
   "address": "57A Lộ Vòng Cung, An Bình, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 9.9994825,
   "Latitude": 105.747925
 },
 {
   "STT": 85,
   "Name": "Nhà thuốc Trung Sơn 8",
   "address": "161 đường 30/4, Xuân Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0338976,
   "Latitude": 105.7782917
 },
 {
   "STT": 86,
   "Name": "Nhà thuốc Phương Lan",
   "address": "162/4 Lộ Vòng Cung, An Bình, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0054204,
   "Latitude": 105.7426786
 },
 {
   "STT": 87,
   "Name": "Nhà thuốc Minh Trung",
   "address": "162 Cách Mạng Tháng 8, Cái Khế, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0516261,
   "Latitude": 105.7716786
 },
 {
   "STT": 88,
   "Name": "Nhà thuốc y học cổ truyền dân tộc Trường An",
   "address": "195 Phan Đình Phùng, Tân An, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0276533,
   "Latitude": 105.783925
 },
 {
   "STT": 89,
   "Name": "Nhà thuốc Đức Minh",
   "address": "144A Cách Mạng Tháng 8, Bùi Hữu Nghĩa, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0556774,
   "Latitude": 105.7676768
 },
 {
   "STT": 90,
   "Name": "Nhà thuốc Minh Tiến",
   "address": "150/4 khu vực 5, An Bình, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0051936,
   "Latitude": 105.7425904
 },
 {
   "STT": 91,
   "Name": "Nhà thuốc Vy Trang",
   "address": "492 Cách Mạng Tháng 8, Bùi Hữu Nghĩa, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0712764,
   "Latitude": 105.7520612
 },
 {
   "STT": 92,
   "Name": "Nhà thuốc Hưng Hân",
   "address": "7 Nguyễn Việt Hồng, An Phú, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0319916,
   "Latitude": 105.7784888
 },
 {
   "STT": 93,
   "Name": "Nhà thuốc Thành Phố Cần Thơ 3",
   "address": "312B Nguyễn Văn Linh, An Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0280249,
   "Latitude": 105.7744199
 },
 {
   "STT": 94,
   "Name": "Nhà thuốc Y học cổ truyền Thiên Phước Đường ",
   "address": "52 Trần Phú, Cái Khế, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.8259717,
   "Latitude": 106.6853088
 },
 {
   "STT": 95,
   "Name": "Nhà thuốc Phương Uyên",
   "address": "36/55 Trần Việt Châu, An Hòa, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.047688,
   "Latitude": 105.7735833
 },
 {
   "STT": 96,
   "Name": "Nhà thuốc Hà Giang",
   "address": "10 Hùng Vương, Thới Bình, Ninh Kiều,Thành Phố Cần Thơ ",
   "Longtitude": 10.049088,
   "Latitude": 105.7722312
 },
 {
   "STT": 97,
   "Name": "Nhà thuốc Phước Hằng",
   "address": "175/5 Lê Hồng Phong, Bình Thủy, Bình Thủy,Thành Phố Cần Thơ",
   "Longtitude": 10.0439894,
   "Latitude": 105.7812671
 },
 {
   "STT": 98,
   "Name": "Nhà thuốc bắc Vạn Trường",
   "address": "55/6 Duy Tân, Lê Bình, Cái Răng,Thành Phố Cần Thơ",
   "Longtitude": 10.005568,
   "Latitude": 105.7491421
 },
 {
   "STT": 99,
   "Name": "Nhà thuốc Trung Tín",
   "address": "174 Lý Tự Trọng, An Cư, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0374506,
   "Latitude": 105.7746695
 },
 {
   "STT": 100,
   "Name": "Nhà thuốc Xuân Tùng",
   "address": "9A1 đường 30 Tháng 4, Xuân Khánh, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0265617,
   "Latitude": 105.7742214
 },
 {
   "STT": 101,
   "Name": "Nhà thuốc Gia Cường",
   "address": "40 Hùng Vương, Thới Bình, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0442907,
   "Latitude": 105.776848
 },
 {
   "STT": 102,
   "Name": "Nhà thuốc Thanh Tuấn",
   "address": "90C quốc lộ 1A, An Bình, Ninh Kiều,Thành Phố Cần Thơ",
   "Longtitude": 10.0114235,
   "Latitude": 105.7497232
 }
];